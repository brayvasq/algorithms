require "option_parser"

# This is a c implementation for factorial sequence
# The factorial sequence is represented by the following formule:
#  Fac(n) = n * Fac(n-1)
# And, we have the following base cases:
#  Fac(0) = 1 according to the convention for an empty product
#  Fac(1) = 1
#
# In C we can use loops, so we will implement the iterative way too.

# Iterative way implementation
module Factorial
  # Iterative way implementation
  def self.iterative(n)
    accumulator = 1.to_u64 # It's the base case
    # for 0 and 1, the result is 1

    (2..n).each do |i|
      accumulator *= i.to_u64 # We can use Assignment operators
    end

    return accumulator
  end

  # Bottom up approach
  # We use a data structure to simulate recursion
  def self.bottom_up(n)
    sequence = [] of UInt64

    (0..n).each do |i|
      if (i == 0 || i == 1)
        sequence << 1.to_u64
      else
        sequence << (i * sequence[i - 1]).to_u64
      end
    end

    return sequence[n]
  end

  # Recursive implementation
  def self.recursive(n : UInt64)
    return 1.to_u64 if n < 2
    n * recursive(n - 1)
  end
end

# Crystal has OptionParser, to pass arguments to a CLI program
# identify them and process them.
OptionParser.parse do |parser|
  parser.on "-i NUM", "--iterative=NUM" do |num|
    x = Factorial.iterative num.to_u64
    puts "Fac(#{num}) = #{x}"
  end

  parser.on "-r NUM", "--recursive=NUM" do |num|
    x = Factorial.recursive num.to_u64
    puts "Fac(#{num}) = #{x}"
  end

  parser.on "-u NUM", "--bottomup=NUM" do |num|
    x = Factorial.bottom_up num.to_u64
    puts "Fac(#{num}) = #{x}"
  end
end
