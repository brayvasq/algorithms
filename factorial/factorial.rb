# This is a c implementation for factorial sequence
# The factorial sequence is represented by the following formule:
#  Fac(n) = n * Fac(n-1)
# And, we have the following base cases:
#  Fac(0) = 1 according to the convention for an empty product
#  Fac(1) = 1
#
# In C we can use loops, so we will implement the iterative way too.

# Iterative way implementation
module Factorial
  # Iterative way implementation
  def self.iterative(n)
    accumulator = 1 # It's the base case
                    # for 0 and 1, the result is 1

    (2..n).each do |i|
      accumulator *= i  # We can use Assignment operators
    end

    return accumulator
  end

  # Bottom up approach
  # We use a data structure to simulate recursion
  def self.bottom_up(n)
    sequence = []
    
    (0..n).each do |i|
      if (i == 0 || i == 1)
        sequence << 1
      else
        sequence << i * sequence[i - 1]
      end
    end

    return sequence[n]
  end

  # Recursive implementation
  def self.recursive(n)
    if(n == 0 || n == 1)
      return 1
    else
      return n * recursive(n - 1)
    end
  end
end
