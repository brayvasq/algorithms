# Factorial
In mathematics, the factorial of a positive integer n, denoted by n!, is the product of all positive integers less than or equal to n.
```bash
n! = n * (n - 1) * (n - 2) * (n - 3) * .... * 3 * 2 * 1

#The value of 0! is 1, according to the convention for an empty product 

# The factorial function is defined by the following recursive function:
Fac(0) = 1
Fac(n) = n * Fac(n - 1).

+-----------------------------+
| Index   | 0 | 1 | 2 | 3 | 4 |
| Sequence| 1 | 1 | 2 | 6 | ? |
+-----------------------------+

Fac(4) = 4 * Fac(4 - 1)      = 4 * Fac(3)
Fac(4) = 4 * (3 * Fac(3 - 1) = 4 * (3 * Fac(2))
Fac(4) = 4 * (3 * (2 * Fac(2 - 1))) = 4 * (3 * (2 * Fac(1))
Fac(4) = 4 * (3 * (2 * 1 * Fac(1 - 1)) = 4 * (3 * (2 * (1 * Fac(0))))
Fac(4) = 4 * (3 * (2 * (1 * 1)) = 24

# Tail way
# As we can see, we are saving in memory values until the 
# las call is done. We can, improve it using an auxiliar variable
# as an accumulator.
Accumulator = 1
Fac(0) = Accumulator

Fac(N) = Fac(N, Accumulator)
Fac(N, Accumulator) = Fac(N - 1, N * Accumulator)

Fac(4) = Fac(4, 1)
Fac(4, 4)  = Fac(4 - 1, 4 * 1)  = Fac(3, 4)
Fac(3, 4)  = Fac(3 - 1, 3 * 4)  = Fac(2, 12)
Fac(2, 12) = Fac(2 - 1, 2 * 12) = Fac(1, 24)
Fac(1, 24) = Fac(1 - 1, 24 * 1) = Fac(0, 24)
Fac(0) = Accumulator = 24


# Bottom-up
# We use a data structure to simulate recursion
# This data will help us to avoid the amount of enviroments
# created by a recursion, however if the data structure
# contains a big amount of data, it still use a big amount of memory

+-----------------------------+
| Sequence| 1 | 1 | 2 | 6 | ? |
+-----------------------------+


# We have to setup with -1 the values that we don't know until n
sequence = [-1, -1, ...... n]

for i = 0; i <= n; i++ {
  if i == 0 {
    sequence[i] = 1
  } else if i == 1 {
    sequence[i] = 1
  } else {
    sequence[i] = i * sequence[i - 1]
  }
}

return sequence[n]
```

Finally, we can calculate the factorial for a number in an iterative way as follow
```bash
accumulator = 1

for i = 2; i <= n; i++ {
  accumulator = accumulator * i
}

return accumulator
```

## Pending
- [ ] Improve printing.
- [ ] Fix big factorial numbers in C.