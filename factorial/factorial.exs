# This is an erlang implementation for factorial sequence
# The factorial sequence is represented by the following formule:
#  Fac(n) = n * Fac(n-1)
# And, we have the following base cases:
#  Fac(0) = 1 according to the convention for an empty product
#  Fac(1) = 1
#
# Elixir has not loops, so it will be a recursive implementation.
defmodule Factorial do
  # Functions pattern matching
  # Each function declaration, is called function clause.
  def fac(0), do: 1

  # Guards, to avoid pattern match when you have a range or
  # you don't know the limit.
  # Guards, are additional clauses to go in a function, to make pattern
  # matching more expresive.
  # function(X) when X >= 16, x =< 104 -> ...
  def fac(n) when n > 0 do
    n * fac(n - 1)
  end

  # Tail implementation: 
  # Is a way to reduce the amount of data to store in memory in a recursive function
  # And try to do the proccess more lineal, like the iterative way, but the iterative
  # way doesn't store data in memory, because don't create new environments

  # This is the entry point, is like the public function to use.
  # And also, sets something like a default value (is like the value of the base case).
  def tail(n), do: tail(n, 1)

  def tail(0, acc), do: acc
  def tail(n, acc) when n > 0 do
    tail(n - 1, n * acc)
  end
end
