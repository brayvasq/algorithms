// This is a c implementation for factorial sequence
// The factorial sequence is represented by the following formule:
//  Fac(n) = n * Fac(n-1)
// And, we have the following base cases:
//  Fac(0) = 1 according to the convention for an empty product
//  Fac(1) = 1
//
// In C we can use loops, so we will implement the iterative way too.

// In c all sentences should end with a semicolon ';'

// Importing libraries headers using #include
// The stdio is the standart library, and provides
// some basic functions.
#include <stdio.h>
#include <stdlib.h> // C Standard Library, cotains type conversion functions, memory management functions, etc.
#include <string.h> // Used to compare strings and string operations.
#include <stdint.h> // Provides a set of tipedefs, i.e: uint64_t.

// We going to use int for this example
// However, for big numbers we should use unsigned long or unsigned long long.
// We can define the headers of functions before the
// main function and their implementatios after the main function.
// However, we can define the functions before the main function
uint64_t iterative(int n);

// BottomUp implementation
uint64_t bottom_up(int n);

// Recursive implementation
uint64_t recursive(int n);

// The entry point of a C program is the main() method
// In C the main method always return an integer.
// The program return 0 if is a normal execution,
// and returns other values if an error occurs
//
// The main function also can receive command line arguments
// represented by the int argc and char** argv parameters
// where argv is the array of parameters and argc is the
// number of parameters
int main(int argc, char **argv)
{
    int status = 0;

    if (argc > 2)
    {
        int n = atoi(argv[2]);
        if (strcmp("--iterative", argv[1]) == 0)
        {
            uint64_t result = iterative(n);
            printf("Fac(%d) = %lu\n", n, result);
        }
        else if (strcmp("--bottomup", argv[1]) == 0)
        {
            uint64_t result = bottom_up(n);
            printf("Fac(%d) = %lu \n", n, result);
        }
        else if (strcmp("--recursive", argv[1]) == 0)
        {
            uint64_t result = recursive(n);
            printf("Fac(%d) = %lu \n", n, result);
        }
    }
    else
    {
        status = 1;
    }

    return status;
}

// Iterative way implementation
uint64_t iterative(int n)
{
    uint64_t accumulator = 1; // It's the base case
                              // for 0 and 1, the result is 1

    int i;
    for (i = 2; i <= n; i++)
    {
        accumulator *= i; // We can use Assignment operators
    }

    return accumulator;
}

// Bottom up approach
// We use a data structure to simulate recursion
uint64_t bottom_up(int n)
{
    uint64_t sequence[n];

    int i;
    for (i = 0; i <= n; i++)
    {
        if (i == 0 || i == 1)
        {
            sequence[i] = 1;
        }
        else
        {
            sequence[i] = i * sequence[i - 1];
        }
    }

    return sequence[n];
}

// Recursive implementation
uint64_t recursive(int n)
{
    if(n == 0 || n == 1)
    {
        return 1;
    }
    else
    {
        return n * recursive(n - 1);
    }
}
