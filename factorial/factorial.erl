%% This is an erlang implementation for factorial sequence
%% The factorial sequence is represented by the following formule:
%%  Fac(n) = n * Fac(n-1)
%% And, we have the following base cases:
%%  Fac(0) = 1 according to the convention for an empty product
%%  Fac(1) = 1
%%
%% Erlang has not loops, so it will be a recursive implementation.

%% In erlang all sentences should end with a period '.'

%% It defines a module
%% The first attribute is the name of the module, and it's an atom.
%% Commonly the name of the file is the name of the module too.
-module(factorial).

%% Exporting functions
%% We export the interface to use the function.
%% The exported function has the following structure:
%%   name/arity -> function_name/number_of_parameters
%% -export([function/0, function/1, function/2]
%% If you want to export all functions you can use:
%% -compile(export_all).
-export([fac/1, tail/1]).

%% Functions pattern matching
%% Each function declaration, is called function clause.
%% And, they must be separated by semicolons ;
fac(0) -> 1;
%% Guards, to avoid pattern match when you have a range or
%% you don't know the limit.
%% , acts as andalso
%% ; acts as orelse
%% Guards, are additional clauses to go in a function, to make pattern
%% matching more expresive.
%% function(X) when X >= 16, x =< 104 -> ...
fac(N) when(N) > 0 -> N * fac(N - 1).

%% Tail implementation: 
%% Is a way to reduce the amount of data to store in memory in a recursive function
%% And try to do the proccess more lineal, like the iterative way, but the iterative
%% way doesn't store data in memory, because don't create new environments

%% This is the entry point, is like the public function to use.
%% And also, sets something like a default value (is like the value of the base case).
tail(N) -> tail(N, 1).

tail(0, Acc) -> Acc;
tail(N, Acc) when(N) > 0 -> tail(N - 1, N * Acc).
