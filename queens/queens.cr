# This is a C implementation for n-queens problem, for N = 4
# This problem is about place N queens, without their threaten each other.
# In the board a queen threat queens that are in the same row, column or diagonal.
#      [X, X, X, _]
#      [X, Q, X, X]    Q = Represents a Queen
#      [X, X, X, _]    _ = Represents empty places
#      [_, X, _, X]    X = Represents where the other queens cannot be placed
#
# We are going to use a vector to represent the Queens position. To avoiding
# use a matrix that will take more memory space.
#
# So, if we have the following array V = [1,2,3,4], we have the following
#      i -> Row
#      V[i] -> Column
# So the above array represents the following board
#      [1, _, _, _]
#      [_, 2, _, _]
#      [_, _, 3, _]
#      [_, _, _, 4]
#

module Queens
  N_QUEENS = 4

  # Checks if there are queens threaten others
  #
  # @return TRUE if there are queens threaten others, FALSE otherwise
  def self.check_diagonals(index, array)
    result = true

    (0...N_QUEENS).each do |i|
      (0...N_QUEENS).each do |k|
        # i != K, represents if the queens aren't in the same row
        if (i != k && array[i] != -1 && array[k] != -1)
          # To determine if a queen is threaten other queen in the
          # diagonals, we have that two queens that are threaten each other
          # have the same (row - column) value and (row + column).
          # So we have the following equations:
          #      i - j = k - l or i + j = k + l
          #      j - l = i - k or j - l = k - i
          # Where i and k = Rows
          # Where j and l = Columns
          if (array[i] - i) == (array[k] - k)
            result = false
            break
          elsif (array[i] + i) == (array[k] + k)
            result = false
            break
          elsif (array[i] - array[k]) == (i - k)
            result = false
            break
          elsif (array[i] - array[k]) == (k - i)
            result = false
            break
          end
        end
      end
    end

    return result
  end

  # Recursive method, that uses backtraking to find a solution
  # @param k [Integer] is the number of queens to place
  # @param array [Array] is the response array
  def self.recursive(k, array)
    if k == N_QUEENS
      p array
    else
      (0...N_QUEENS).each do |i|
        unless array.includes?(i + 1)
          array[k] = i + 1
          recursive(k + 1, array) if check_diagonals(i, array)
          array[k] = -1
        end
      end
    end
  end
end

solution = [-1, -1, -1, -1] of Int32
Queens.recursive 0, solution
