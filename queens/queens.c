// This is a C implementation for n-queens problem, for N = 4
// This problem is about place N queens, without their threaten each other.
// In the board a queen threat queens that are in the same row, column or diagonal.
//      [X, X, X, _]
//      [X, Q, X, X]    Q = Represents a Queen
//      [X, X, X, _]    _ = Represents empty places
//      [_, X, _, X]    X = Represents where the other queens cannot be placed
//
// We are going to use a vector to represent the Queens position. To avoiding
// use a matrix that will take more memory space.
//
// So, if we have the following array V = [1,2,3,4], we have the following
//      i -> Row
//      V[i] -> Column
// So the above array represents the following board
//      [1, _, _, _]
//      [_, 2, _, _]
//      [_, _, 3, _]
//      [_, _, _, 4]
//

// In c all sentences should end with a semicolon ';'

// Importing libraries headers using #include
// The stdio is the standart library, and provides
// some basic functions.
#include <stdio.h> // standard input-output.

// Board size and number of queens to place
#define N_QUEENS 4


// We use the keyword #define to create constants
// The constats should be defined in uppercase and
// uses snake case to separate multiple words, i.e: MAX_SIZE

// All items different to 0 is TRUE
#define TRUE 1
#define FALSE 0

// Printing a vector
void print_array(int *array);

// Recursive algorithm
void queens(int k, int *array);

// Checks if an array contains a value
int contains(int value, int *array);

// Check if there are queens threaten other
int checkDiagonals(int value, int *array);

// The entry point of a C program is the main() method
// In C the main method always return an integer.
// The program return 0 if is a normal execution,
// and returns other values if an error occurs
//
// The main function also can receive command line arguments
// represented by the int argc and char** argv parameters
// where argv is the array of parameters and argc is the
// number of parameters
int main()
{
    int status = 0;
    int solution[4] = {-1, -1, -1, -1};
    printf("N_QUEENS: %d\n", N_QUEENS);
    print_array(solution);
    queens(0, solution);
    return 0;
}

// Iterates an array and print each element of it
void print_array(int *array)
{
    int i = 0;
    
    for(i = 0; i < 4; i++){
        printf("%d ", array[i]);
    }

    printf("\n");
}

// Iterates an arary and check if it contains a value
//
// @return TRUE if the array contains the value, FALSE otherwise
int contains(int value, int *array)
{
    // size_t size = sizeof(array) / sizeof(array[0]);
    int found = FALSE;
    int i = 0;

    while(!found && i < N_QUEENS)
    {
        if(value == array[i])
        {
            found = TRUE;
        }

        i++;
    }
    return found;
}

// Checks if there are queens threaten others
//
// @return TRUE if there are queens threaten others, FALSE otherwise
int checkDiagonals(int index, int *array)
{
    int result = TRUE;
    int i = 0;
    int k = 0;

    for(i = 0; i < N_QUEENS; i++)
    {
        for(k=0; k < N_QUEENS; k++)
        {
            // i != K, represents if the queens aren't in the same row
            if(i != k && array[i] != -1 && array[k] != -1){

                // To determine if a queen is threaten other queen in the
                // diagonals, we have that two queens that are threaten each other
                // have the same (row - column) value and (row + column). 
                // So we have the following equations:
                //      i - j = k - l or i + j = k + l
                //      j - l = i - k or j - l = k - i
                // Where i and k = Rows
                // Where j and l = Columns
                if((array[i] - i) == (array[k] - k))
                {
                    result = FALSE;
                    break;
                } 
                else if ((array[i] + i) == (array[k] + k))
                {
                    result = FALSE;
                    break;
                }
                else if ((array[i] - array[k]) == (i - k))
                {
                    result = FALSE;
                    break;
                }
                else if ((array[i] - array[k]) == (k - i))
                {
                    result = FALSE;
                    break;
                }
            }
        }
    }

    return result;
}

// Recursive method, that uses backtraking to find a solution
void queens(int k, int *array)
{
    if(k == N_QUEENS)
    {
        print_array(array);
    }
    else
    {
        int i = 0;
        int j = 0;
        for(i = 0; i < N_QUEENS; i++)
        {
            if(!contains(i + 1, array))
            {
                array[k] = (i + 1);
                if(checkDiagonals(i, array))
                {
                    queens(k + 1, array);
                }
                array[k] = -1;
            }
        }
    }
}
