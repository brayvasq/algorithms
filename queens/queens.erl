%% NOTE: This code doesn't work,
-module(queens).

-export([recursive/0]).

recursive() -> loop(0, 0, []).

loop(_, 4, Queens) -> Queens;
loop(N, I, Queens) when I < 4 ->
    recursive(N, I, Queens),
    loop(N, I + 1, Queens).

recursive(4, _, Queens) -> Queens;
recursive(N, I, Queens) when N < 4 ->
    case not lists:member((I + 1), Queens) of
        true ->
            Temp = [Queens | [I+1]],
            recursive(N + 1, I, Queens);
        false -> Queens
    end.


