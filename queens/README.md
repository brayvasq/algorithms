# N-Queens
This is a C implementation for n-queens problem, for N = 4
This problem is about place N queens, without their threaten each other.

In the board a queen threat queens that are in the same row, column or diagonal.
```bash
#      [X, X, X, _]
#      [X, Q, X, X]    Q = Represents a Queen
#      [X, X, X, _]    _ = Represents empty places
#      [_, X, _, X]    X = Represents where the other queens cannot be placed
```
We are going to use a vector to represent the Queens position. To avoiding use a matrix that will take more memory space.
```bash
# So, if we have the following array V = [1,2,3,4], we have the following
#      i -> Row
#      V[i] -> Column
# So the above array represents the following board
#      [1, _, _, _]
#      [_, 2, _, _]
#      [_, _, 3, _]
#      [_, _, _, 4]
```

To determine if a queen is threaten other queen in the diagonals, we have that two queens that are threaten each other have the same (row - column) value and (row + column). 
```bash
#   /									\
#  /       Row + Column					 \		Row - Column = 0
# / 									  \

# NOTE: Count from 0 until N - 1, for positions

# So we have the following equations.
# Same diagonal for Q1 = (i, j) and Q2 = (k, l) if:
#      i - j = k - l or i + j = k + l
#      j - l = i - k or j - l = k - i
# Where i and k = Rows
# Where j and l = Columns

# So having the following vectors we could build a board
# and checks if the queens aren't threaten each other

# V = [1, 3, 2, 4] 
#
#     [1, _, _, _]   We can see, that 3 and 2 are threaten:
#     [_, _, 3, _]   i(2) - j(3) = k(3) - l(2) -> -1 = 1  ERROR
#     [_, 2, _, _]   i(2) + j(3) = k(3) + l(2) -> 5 = 5 OK 
#     [_, _, _, 4]            (Queens threaten)
#

# V = [2, 4, 1, 3] 
#
#     [_, 2, _, _]   We can see, that 3 and 2 are threaten:
#     [_, _, _, 4]   i(1) - j(2) = k(2) - l(4) -> -1 = -2  ERROR
#     [1, _, _, _]   i(1) + j(2) = k(2) + l(4) -> 3 = 6 ERROR
#     [_, _, 3, _]   j(2) - l(4) = i(1) - k(2) -> -2 = -1 ERROR
#                    j(2) - l(4) = k(2) - i(1) -> -2 = 1 ERROR
#                               (This positionig is valid)
```

## Pending
- [ ] Improve printing.
- [ ] Add tree to documentation.
- [ ] Erlang algorithm.
