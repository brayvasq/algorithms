# Algorithms
This repository will contains some implementations of some algorithms using manilty erlang and c.

## List
- `fibonacci/` Contains the fibonacci sequence implementation.
- `factorial/` Contains the factorial implementation.
- `queens/` Contains the n-queens problem implementation. (N = 4)
- `lists` Contains the following algorithms:
  - `size` return the size of a lists, using recursion
  - `reverse` return the list reversed
  - `sublist` return a sublist from a list
  - `duplicate` duplicate elements form a list
  - `zipping` merge two lists in a list of pair tuples.

## Run
```bash
# Running Erlang programs
erl> cd('your/file/path').
erl> compile('your_module').
erl> your_module:your_function().

# Running C programs
gcc your_program.c -o your_program.out
./your_program.out

## Compile with some optimizations
gcc your_program.c -o your_program.out -O3

## Or if you would like to interact with the program
./your_program.out --function param

# Running Elixir scripts
elixir my_script.exs
## Interactive way
iex my_script.exs
iex> Module.function

# Running Ruby scripts
ruby my_script.rb
## Intertactive way
irb> require './my_script'
irb> Module.function

# Running Crystal scripts
crystal run my_script.cr -p VAL
## Build script
crystal build my_script.cr -o your_program.out
## Release build script
crystal build my_script.cr --release --no-debug -o your_program.out
## Run compiled crystal
./my_script.out -p VAL
```