```bash
# Vector solution:
C = [1, 2, 3] -> Coins Values
V = 4 -> Value to change
N = 3 -> Vector Size
M = 3 -> Number of different coins

# Recursive way
coins(C, V, N)
  if C[N] > V:
    coins(C, V, N-1)
  else
    min(coins(C, V, N - 1), coins(C, V - C[N])+1)
    
C[N] > V -> coins(N - 1, V)
C[N] <= V -> min(coins(N - 1, V), coins(N, V - C[N]) + 1)

0 <> N -> 0 = coins(N, 0)
0 <= C -> Infinity = coins(0, C)

# Dinamic way
V = [1, 2, 3] -> Coins vector
U = [-1, -1, -1] -> Units vector

Iterate Coins vector -> i
Iterate Units vector -> j

if i >= coins[j]
 T[i] = min(T[i], 1 + T[i - coin[j]]) 

### Fixed dynamic programming
### Video: https://www.youtube.com/watch?v=jaNZ83Q3QGc
AMOUNT: 12
COINS: 1 2 5

# Array with size of AMOUNT
 0  1  2  3  4  5  6  7  8  9  10 11 12
[  |  |  |  |  |  |  |  |  |  |  |  |  ]  = COMBINATIONS

## The following will give us the number of combinations to get the AMOUNT using the COINS
if AMOUNT >= COIN then
 COMBINATIONS[AMOUNT] += COMBINATIONS[AMOUNT - COIN]
 
CURRENT COIN: 1
[ 1 1 2 2 3 3 4 4 5 5 6 6 7]

## Video for tell the coins quantity https://www.youtube.com/watch?v=rdI94aW6IWw
```
