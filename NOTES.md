```bash
# Coins problem. Bottom Up
## Dinamyc programming.
## https://www.youtube.com/watch?v=NJuKJ8sasGk

Total = 13
Coins = 7, 2, 3, 6

# Array of totals
T = []

for j from 0...coins
  for i from 0...Total
    # min(Sin usar la moneda, Usando la moneda)
    if i >= coins[j]
      T[i] = min(T[i], 1 + T[i - coins[j]])

# Step by Step
Coins
 0 1 2 3   # j -> index
[7 2 3 6]

# Aquí se guarda min(T[i], 1 + T[i - coins[j]])
 0  1  2  3  4  5  6  7  8  9  10 11 12 13 # i -> index
[0  &  &  &  &  &  &  &  &  &  &  &  &  &]  # & = infinito

# Aquí se guarda j => para indicar la posicion de la moneda que se usó.
 0  1  2  3  4  5  6  7  8  9 10 11 12 13
[0 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1] 

# Se recorre i y se valida la condicion i >= coins[j]

# i >= coins[j]
coins[j] = 7
0..6 >=  7 false
7..13 >= 7 true

# min(T[i], 1 + T[i - coins[j]])
min(&, 1 + T[7 - 7]) => min(&, 1)

 0  1  2  3  4  5  6  7  8  9  10 11 12 13 # i -> index
[0  &  &  &  &  &  &  1  &  &  &  &  &  &]  # & = infinito

 0  1  2  3  4  5  6  7  8  9 10 11 12 13
[-1 -1 -1 -1 -1 -1 -1  0 -1 -1 -1 -1 -1 -1]

NOTA= min(&, 1 + &) = &

# Vector final.
 0  1  2  3  4  5  6  7  8  9  10 11 12 13 # i -> index
[0  &  1  1  2  2  1  1  2  2  2  3  2  2]  # & = infinito

  0  1  2  3  4  5  6  7  8  9 10 11 12 13
[-1 -1  1  2  1  2  3  0  3  1  2  1  3  3]

# Camino
T[Total] = T[13] = 3 => Coins[3] = 6
T[Total - 6] = T[7] = 0 => Coins[0] = 7
T[Total - 6 - 7] = T[0] => # si i es 0 entonces el vector respuesta está completo

respuesta = [6, 7]
```

```bash
# Coins problem. Bottom Up tablas.
# https://www.youtube.com/watch?v=tk1cccLH_BQ
i = Denominaciones
j = Cantidades

Cambio(i, j)
1 <= i <= n
0 <= j <= m

# Condiciones
0 if (j=0)
& if (j < di && i = 1)
Cambio(i-1, j) if (j < di && i > 1)
1 + Cambio(i, j-di) if (j >= di && i = 1)

min(Cambio(i - 1, j), 1 + Cambio(i, j-di)) if (j >= di && i > 1)

# Step by step
 d1 d2 d3  # Denominaciones
[ 1  3  5 ]

M = 5 # Cantidad
     0  1  2  3  4 # j -> index
d1 [ _  _  _  _  _ ]
d2 [ _  _  _  _  _ ]
d3 [ _  _  _  _  _ ]
#i -> index

# Se llena los casos base
     0  1  2  3  4  5 
d1 [ 0  _  _  _  _  _ ]
d2 [ 0  _  _  _  _  _ ]
d3 [ 0  _  _  _  _  _ ]

# Tabla final
     0   1   2   3   4   5   # => j
d1 [ 0  1+0 1+1 1+2 1+3 1+4 ]
d2 [ 0   1   2  1+0 1+1 1+2 ]
d3 [ 0   1   2   1   2  1+0 ] # Desde el últmo valor se empieza a armar la respuesta
# => i
# => M - d3 -> 5 - 5 = 0
# Camino
[ 0  0  1 ]

# Para 4
  i  j
T[2][4] = 2 => d3 <= 4 NO
T[1][4] = 2 => d2 <= 4 SI

[ 0  1  0 ]

4 - d2 = 4 - 3 = 1
T[1][1] = 1 => d2 <= NO
T[0][1] = 1 => d1 <= SI

[0  1  0]
1 - d1 = 0 => Caso base, respuesta.
```
