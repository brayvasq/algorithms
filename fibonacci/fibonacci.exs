# This is an elixir implementation for fibonacci sequence
# The fibonacci sequence is represented by the following formule:
#   Fib(n) = Fib(n - 1) + Fib(n - 2)
# And, We have the following base cases:
#   Fib(0) = 0
#   Fib(1) = 1
#
# For the tail way, we will define the following equation:
#   Fib(n) = Fib(n, previous, current) = Fib(n, 1, 0)
#   Fib(n, previous, purrent) = Fib(n - 1, previous + current, Previous) 
# And, We have the following base cases:
#   Fib(0) = Current
#   Fib(1) = Previous
# As Erlang, Elixir has not loops, so it will be a recursive implementation.

# We can see that, we don't have to export the functions like erlang

# defmodule Name is the equivalent to -module(name). in erlang
# here we can user variables in downcase, because in elixir the atoms
# are represented as :atom
defmodule Fibonacci do
  # In Elixir also we have functions pattern matching
  # Each function declaration, is called function clause.
  # And, they we don't need to use semicolons ; to separate each other
  # we use do and end like ruby sintax.
  def fib(0), do: 0
  def fib(1), do: 1   
  # In Elixir as in erlang, we have Guards
  # Guards, to avoid pattern match when you have a range or
  # you don't know the limit.
  # Guards, are additional clauses to go in a function, to make pattern
  # matching more expresive.
  # function(X) when X >= 16, x =< 104 -> ...
  def fib(n) when n > 1 do
    fib(n-1) + fib(n-2)
  end

  # Tail implementation: 
  # Is a way to reduce the amount of data to store in memory in a recursive function
  # And try to do the proccess more lineal, like the iterative way, but the iterative
  # way doesn't store data in memory, because don't create new environments

  # This is the entry point, is like the public function to use.
  # And also, sets something like a default value (is like the value of the base case).
  def tail(n), do: tail(n, 1, 0)

  def tail(0, _, current), do: current
  def tail(1, previous, _), do: previous
  def tail(n, previous, current) when n > 1 do
    tail(n - 1, previous + current, previous)
  end
end
