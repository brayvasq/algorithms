require "option_parser"

# This is a Crystal implementation for fibonacci sequence
# The fibonacci sequence is represented by the following formule:
#   Fib(n) = Fib(n - 1) + Fib(n - 2)
# And, We have the following base cases:
#   Fib(0) = 0
#   Fib(1) = 1
#
# For the tail way, we will define the following equation:
#   Fib(n) = Fib(n, previous, current) = Fib(n, 1, 0)
#   Fib(n, previous, purrent) = Fib(n - 1, previous + current, Previous)
# And, We have the following base cases:
#   Fib(0) = Current
#   Fib(1) = Previous
# In Crsytal we can use loops, so we will implement the iterative way too.

# As golang, in Crystal we can omit the datatypes, however
# is better if we tell the datatype
module Fibonacci
  # Recusrive implementation
  def self.recursive(n)
    if n == 0
      return 0
    elsif n == 1
      return 1
    else
      return recursive(n - 1) + recursive(n - 2)
    end
  end

  # Iterative way implementation
  def self.iterative(n)
    previous = 0
    current = 1
    aux = 0

    # 1...n from 1 to n excluding n
    # 1..n from 1 to n including n
    (0..n).each do
      aux = previous
      previous = current
      current = aux + current
    end

    return n > 0 ? current : previous
  end

  # Tail implementation
  # Is a way to reduce the amount of data to store in memory in a recursive function
  # And try to do the proccess more lineal, like the iterative way, but the iterative
  # way doesn't store data in memory, because don't create new environments
  def self.tail(n)
    tail_fibonacci(n, 1, 0)
  end

  # We have to create two differents functions
  # because C doesn't allow polymorphism
  def self.tail_fibonacci(n, previous, current)
    if n == 0
      return current
    elsif n == 1
      return previous
    else
      return tail_fibonacci(n - 1, previous + current, previous)
    end
  end

  # TopDown implementation:
  # In this approach we still use recursion, but avoid creating
  # unnecessary environments.
  # We will use a data structure to store the known values.
  def self.top_down(n)
    sequence = [] of Int32

    (0..n).each do
      sequence << -1
    end

    return fib_top_down(sequence, n)
  end

  # TopDown recursive function
  def self.fib_top_down(sequence, n)
    if (sequence[n] != -1)
      return sequence[n]
    else
      if (n == 0 || n == 1)
        sequence[n] = n
      else
        sequence[n] = fib_top_down(sequence, n - 1) + fib_top_down(sequence, n - 2)
      end
    end

    return sequence[n]
  end

  # Bottom up approach
  # We use a data structure to simulate recursion
  def self.bottom_up(n)
    sequence = [] of Int32

    (0..n).each do |i|
      if (i == 0 || i == 1)
        sequence << i
      else
        sequence << sequence[i - 1] + sequence[i - 2]
      end
    end

    return sequence[n]
  end
end

# Crystal has OptionParser, to pass arguments to a CLI program
# identify them and process them.
OptionParser.parse do |parser|
  parser.on "-i NUM", "--iterative=NUM" do |num|
    x = Fibonacci.iterative num.to_i
    puts "Fib(#{num}) = #{x}"
  end

  parser.on "-r NUM", "--recursive=NUM" do |num|
    x = Fibonacci.recursive num.to_i
    puts "Fib(#{num}) = #{x}"
  end

  parser.on "-t NUM", "--tail=NUM" do |num|
    x = Fibonacci.tail num.to_i
    puts "Fib(#{num}) = #{x}"
  end

  parser.on "-d NUM", "--topdown=NUM" do |num|
    x = Fibonacci.top_down num.to_i
    puts "Fib(#{num}) = #{x}"
  end

  parser.on "-u NUM", "--bottomup=NUM" do |num|
    x = Fibonacci.bottom_up num.to_i
    puts "Fib(#{num}) = #{x}"
  end
end
