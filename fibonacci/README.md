# Fibonacci sequence
In mathematics, it is an infinite sequence of natural numbers:
```bash
# 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597 ....

  +-----------------------------------+
  |                                   |
14|                                   |
  |                          x        |
12|                                   |
  |                                   |
 8|                      x            |
  |                                   |
 6|                                   |
  |                  x                |
 4|                                   |
  |              x                    |
 2|          x                        |
  |  x   x                            |
  +-----------------------------------+
     1   2   3   4   5   6   7   8   9
```

The sequence begin with 0 and 1;  Each number after that is equal to adding the two numbers right before it together. i.e:
```bash
# We have this:
Fib(0) = 0
Fib(1) = 0

# So, if we want to calculate the fibonacci number for 2, we have to add the fibonacci of the two previous numbers.
# We know the fibonacci number for 0 and 1.
Fib(2) = Fib(1) + Fib(0)
Fib(2) = 1 + 0
Fib(2) = 1
```
We can define this sequence as a recursion, as follows:
```bash
Fib(2) = Fib(2 - 1) + Fib(2 - 2)
Fib(2) = Fib(1) + Fib(0)

# So, in general terms, we can represent the sequence using the following formula:
Fib(n) = Fib(n-1) + Fib(n-2)

# However this way isn't unoptimal, because you will have to store
# many values in memory.
+-----------------------------+
| Index   | 0 | 1 | 2 | 3 | 4 |
| Sequence| 0 | 1 | 1 | 2 | ? |
+-----------------------------+

# If we want to know the fibonacci of 4, we first have to know
# The fibonacci of the previous numbers
Fib(4) = Fib(4 - 1) + Fib(4 - 2)
Fib(4) = Fib(2) + Fib(2)

# However at the beginning we don't have the previous values
# So we have to keep asking for previous values, until we get an asnwer
                            +------+
                            |fib(4)|
                            +------+
                   v--------+      +--------v
               +------+                  +------+
               |fib(3)|                  |fib(2)|
               +------+                  +------+
          v----+      +----v        v----+      +----v
       +------+        +------+  +------+        +------+
       |fib(2)|        |fib(1)|  |fib(1)|        |fib(0)|
       +------+        +------+  +------+        +------+
   v---+      +---v
+------+     +------+
|fib(1)|     |fib(0)|
+------+     +------+

# As we can see, there are some values ​​that are calculated multiple times.
# We can use the tail way, to improve how to calculate the Fibonacci sequence in a more linear way.
# Basically, we will start calculating its value from the base case first.
+-----------------------------+
| Index   | 0 | 1 | 2 | 3 | 4 |
| Sequence| 0 | 1 | 1 | 2 | ? |
| Tail    | ? | 2 | 1 | 1 | 0 |
+-----------------------------+

# We can define the following function with a single recursive call, making it linear 
# and with auxiliary variables that work as an accumulator to avoid saving unnecessary data in memory
current  = 0
previous = 1
Fib(n) = Fib(n, previous, current) = Fib(n, 1, 0)
Fib(n, previous, purrent) = Fib(n - 1, previous + current, Previous) 

# And the base cases are the following
Fib(0) = Current # 0
Fib(1) = Previous # 1

      +---------------------+
      |fib(4) = fib(4, 1, 0)|
      +----------+----------+
                 |
                 v
+------+------+--+------------------+
|fib(4 - 1, 1 + 0, 1) = fib(3, 1, 1)|
+------+------+--+------------------+
                 |
                 v
+------+------+--+------------------+
|fib(3 - 1, 1 + 1, 1) = fib(2, 2, 1)|
+------+------+--+------------------+
                 |
                 v
+------+------+--+------------------+
|fib(2 - 1, 2 + 1, 2) = fib(1, 3, 2)|
+------+------+--+------------------+
                 |
                 v
            +----+-----+
            |fib(1) = 3|
            +----------+
```

We also can use dynamic programing
```bash
# To use dynamic programing, we have the following
# conditions:
# - The problem can be divided in sub-problems
# - The problems must be dependent
# - Use a data structure to store the known values

# Optimal principle: a problem solution must be composed of the solutions of its subproblems 
# and the solutions of its subproblems must be optimal

# Bottom-up
# We use a data structure to simulate recursion
# This data will help us to avoid the amount of enviroments
# created by a recursion, however if the data structure
# contains a big amount of data, it still use a big amount of memory
+-----------------------------+
| Sequence| 0 | 1 | 1 | 2 | ? |
+-----------------------------+


# We have to setup with -1 the values that we don't know until n
sequence = [-1, -1, ...... n]

for i = 0; i <= n; i++ {
  if i == 0 {
    sequence[i] = 0
  } else if i == 1 {
    sequence[i] = 1
  } else {
    sequence[i] = sequence[i - 1] + sequence[i - 2]
  }
}

return sequence[n]

# Top-Down
# In this approach we still use recursion, but avoid creating
# unnecessary environments.
# We will use a data structure to store the known values.
# We also use a wrap function to setup the data structure
# and to call the recursive function.
# It is like recursive way + memoization
Fib(n) {
  sequence = [-1, -1, ...... n]
  return topdown(sequence, n)
}

TopDown(sequence, n){
  if sequence[n] != -1 {
    return sequence[n]
  } else {
    if n == 0 or n == 1 {
      sequence[n] = n
    } else {
      sequence[n] = TopDown(sequence, n - 1) + TopDown(sequence, n - 2)
    }
  }

  return sequence[n]
}
```

Finally, we can calculate the fibonacci sequence in an iterative way as follow
```bash
previous = 0
current = 1

for i = 0; i < n; i++ {
  aux = previous
  previous = current
  current = current + aux
}

return current
```

## Pending
- [ ] Improve printing.
- [ ] Fix big fibonacci numbers in C.
- [ ] Improve topdown performace erlang.
