# This is a Ruby implementation for fibonacci sequence
# The fibonacci sequence is represented by the following formule:
#   Fib(n) = Fib(n - 1) + Fib(n - 2)
# And, We have the following base cases:
#   Fib(0) = 0
#   Fib(1) = 1
#
# For the tail way, we will define the following equation:
#   Fib(n) = Fib(n, previous, current) = Fib(n, 1, 0)
#   Fib(n, previous, purrent) = Fib(n - 1, previous + current, Previous)
# And, We have the following base cases:
#   Fib(0) = Current
#   Fib(1) = Previous
# In Ruby we can use loops, so we will implement the iterative way too.

# In Ruby, we have dynamic types, so we don't have to tell the data types.
# Also, Ruby is an interpreted lang
module Fibonacci
  # Recusrive implementation
  def self.recursive(n)
    if n == 0
      return 0
    elsif n == 1
      return 1
    else
      return recursive(n - 1) + recursive(n - 2)
    end  
  end

  # Iterative way implementation
  def self.iterative(n)
    previous = 0
    current = 1
    aux = 0

    # 1...n from 1 to n excluding n
    # 1..n from 1 to n including n
    for i in 1...n
      aux = previous
      previous = current
      current = aux + current
    end

    return n > 0 ? current : previous
  end

  # Tail implementation
  # Is a way to reduce the amount of data to store in memory in a recursive function
  # And try to do the proccess more lineal, like the iterative way, but the iterative
  # way doesn't store data in memory, because don't create new environments
  def self.tail(n)
    tail_fibonacci(n, 1, 0)
  end

  # We have to create two differents functions
  # because C doesn't allow polymorphism
  def self.tail_fibonacci(n, previous, current)
    if n == 0
      return current
    elsif n == 1
      return previous
    else
      return tail_fibonacci(n - 1, previous + current, previous) 
    end
  end

  # TopDown implementation:
  # In this approach we still use recursion, but avoid creating
  # unnecessary environments.
  # We will use a data structure to store the known values.
  def self.top_down(n)
    sequence = []
    for i in 0..n
      sequence.push(-1)
    end

    return fib_top_down(sequence, n)
  end

  # TopDown recursive function
  def self.fib_top_down(sequence, n)
    if (sequence[n] != -1)
      return sequence[n]
    else
      if(n == 0 || n == 1)
        sequence[n] = n
      else
        sequence[n] = fib_top_down(sequence, n - 1) + fib_top_down(sequence, n - 2)  
      end
    end

    return sequence[n]
  end

  # Bottom up approach
  # We use a data structure to simulate recursion
  def self.bottom_up(n)
    sequence = []

    for i in 0..n
      if(i == 0 || i == 1)
        sequence.push i
      else
        sequence.push sequence[i - 1] + sequence[i - 2]  
      end
    end

    return sequence[n]
  end
end
