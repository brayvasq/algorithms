%% This is an erlang implementation for fibonacci sequence
%% The fibonacci sequence is represented by the following formule:
%%   Fib(n) = Fib(n - 1) + Fib(n - 2)
%% And, We have the following base cases:
%%   Fib(0) = 0
%%   Fib(1) = 1
%%
%% For the tail way, we will define the following equation:
%%   Fib(n) = Fib(n, previous, current) = Fib(n, 1, 0)
%%   Fib(n, previous, purrent) = Fib(n - 1, previous + current, Previous) 
%% And, We have the following base cases:
%%   Fib(0) = Current
%%   Fib(1) = Previous
%% Erlang has not loops, so it will be a recursive implementation.

%% In erlang all sentences should end with a period '.'

%% You can define how to compile a module using the following sentence
%% -compile([debug_info, export_all])
%% Wheere 'debug_info' and 'export_all' are flags or options.

%% It defines a module
%% The first attribute is the name of the module, and it's an atom.
%% Commonly the name of the file is the name of the module too.
-module(fibonacci).

%% Exporting functions
%% We export the interface to use the function.
%% The exported function has the following structure:
%%   name/arity -> function_name/number_of_parameters
%% -export([function/0, function/1, function/2]
%% If you want to export all functions you can use:
%% -compile(export_all).
-export([fib/1, tail/1, top_down/1]).

%% Functions pattern matching
%% Each function declaration, is called function clause.
%% And, they must be separated by semicolons ;
fib(0) -> 0;
fib(1) -> 1;
%% Guards, to avoid pattern match when you have a range or
%% you don't know the limit.
%% , acts as andalso
%% ; acts as orelse
%% Guards, are additional clauses to go in a function, to make pattern
%% matching more expresive.
%% function(X) when X >= 16, x =< 104 -> ...
fib(N) when(N) > 1 -> fib(N - 1) + fib(N - 2).

%% Tail implementation: 
%% Is a way to reduce the amount of data to store in memory in a recursive function
%% And try to do the proccess more lineal, like the iterative way, but the iterative
%% way doesn't store data in memory, because don't create new environments

%% This is the entry point, is like the public function to use.
%% And also, sets something like a default value (is like the value of the base case).
tail(N) -> tail(N, 1, 0).

tail(0, _, Current) -> Current;
tail(1, Previous, _) -> Previous;
tail(N, Previous, Current) when(N) > 1 -> tail(N - 1, Previous + Current, Previous).

%% TopDown implementation:
%% In this approach we still use recursion, but avoid creating
%% unnecessary environments.
%% We will use a data structure to store the known values.

%% TopDown recursive function
%% Using get and put, that are BIf's to handle the process dictionary.
top_down(N) ->
    case get(N) of
        undefined ->
            if N =:= 0 orelse N =:= 1 -> X = N, put(N, X), X;
               true -> 
                   X = fib(N-1) + fib(N-2),
                   put(N, X),
                   X
            end;
        X -> X
    end.
