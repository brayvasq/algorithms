// This is a c implementation for fibonacci sequence
// The fibonacci sequence is represented by the following formule:
//   Fib(n) = Fib(n - 1) + Fib(n - 2)
// And, We have the following base cases:
//   Fib(0) = 0
//   Fib(1) = 1
//
// For the tail way, we will define the following equation:
//   Fib(n) = Fib(n, previous, current) = Fib(n, 1, 0)
//   Fib(n, previous, purrent) = Fib(n - 1, previous + current, Previous)
// And, We have the following base cases:
//   Fib(0) = Current
//   Fib(1) = Previous
//
// In C we can use loops, so we will implement the iterative way too.

// In c all sentences should end with a semicolon ';'

// Importing libraries headers using #include
// The stdio is the standart library, and provides
// some basic functions.
#include <stdio.h>  // standard input-output.
#include <stdlib.h> // C Standard Library, cotains type conversion functions, memory management functions, etc.
#include <string.h> // Used to compare strings and string operations.
#include <stdint.h> // Provides a set of tipedefs, i.e: uint64_t.

// We use the keyword #define to create constants
// The constats should be defined in uppercase and
// uses snake case to separate multiple words, i.e: MAX_SIZE
#define SIZE 60

// We going to use int for this example
// However, for big numbers we should use unsigned long or unsigned long long.
// We can define the headers of functions before the
// main function and their implementatios after the main function.
// However, we can define the functions before the main function.
uint64_t iterative(int n);

// Recusrive implementation
uint64_t recursive(int n)
{
    if (n == 0)
    {
        return 0;
    }
    else if (n == 1)
    {
        return 1;
    }
    else
    {
        return recursive(n - 1) + recursive(n - 2);
    }
}

// Tail implementation
uint64_t tail(int n);
uint64_t tail_fibonacci(int n, uint64_t previous, uint64_t current);

// TopDown implementation
// Wrapper function
uint64_t top_down(int n);
// Recursive function
// TODO: Send a sequence array as reference param
uint64_t fib_top_down(uint64_t *sequence, int n);
// This is the data structure defined to use in
// the topdown approach
// int sequence[SIZE];

// BottomUp implementation
uint64_t bottom_up(int n);

// The entry point of a C program is the main() method
// In C the main method always return an integer.
// The program return 0 if is a normal execution,
// and returns other values if an error occurs
//
// The main function also can receive command line arguments
// represented by the int argc and char** argv parameters
// where argv is the array of parameters and argc is the
// number of parameters
int main(int argc, char **argv)
{
    int status = 0;

    if (argc > 2)
    {
        int n = atoi(argv[2]);
        if (strcmp("--iterative", argv[1]) == 0)
        {
            uint64_t  result = iterative(n);
            printf("Fib(%d) = %lu\n", n, result);
        }
        else if (strcmp("--recursive", argv[1]) == 0)
        {
            uint64_t  result = recursive(n);
            printf("Fib(%d) = %lu \n", n, result);
        }
        else if (strcmp("--tail", argv[1]) == 0)
        {
            uint64_t  result = tail(n);
            printf("Fib(%d) = %lu \n", n, result);
        }
        else if (strcmp("--topdown", argv[1]) == 0)
        {
            uint64_t  result = top_down(n);
            printf("Fib(%d) = %lu \n", n, result);
        }
        else if (strcmp("--bottomup", argv[1]) == 0)
        {
            uint64_t  result = bottom_up(n);
            printf("Fib(%d) = %lu \n", n, result);
        }
    }
    else
    {
        status = 1;
    }

    return status;
}

// Iterative way implementation
// we should decare it as an unsigned int, because fibonacci sequence
// works with prositive natural numbers.
// TODO: however, it should be declared as long or bigint
uint64_t iterative(int n)
{
    uint64_t previous = 0; // This is the base value for fib(0)
    uint64_t current = 1;  // This is the base value for fib(1)
    uint64_t aux = 0;      // This is an auxiliar that will help to
                           // change values

    int i = 0;
    for (i = 1; i < n; i++)
    { // We ignore the base case
        aux = previous;
        previous = current;
        current = aux + current;
    }

    // This is a ternary operator
    // We check if the function calculates
    // the fib(0)
    return n > 0 ? current : previous;
}

// Tail implementation
// Is a way to reduce the amount of data to store in memory in a recursive function
// And try to do the proccess more lineal, like the iterative way, but the iterative
// way doesn't store data in memory, because don't create new environments
uint64_t tail(int n)
{
    tail_fibonacci(n, 1, 0);
}

// We have to create two differents functions
// because C doesn't allow polymorphism
uint64_t tail_fibonacci(int n, uint64_t previous, uint64_t current)
{
    if (n == 0)
    {
        return current;
    }
    else if (n == 1)
    {
        return previous;
    }
    else
    {
        return tail_fibonacci(n - 1, previous + current, previous);
    }
}

// TopDown implementation:
// In this approach we still use recursion, but avoid creating
// unnecessary environments.
// We will use a data structure to store the known values.
uint64_t top_down(int n)
{
    uint64_t sequence[n];

    int i;
    for (i = 0; i <= n; i++)
    {
        sequence[i] = -1;
    }

    return fib_top_down(sequence, n);
}

// TopDown recursive function
uint64_t fib_top_down(uint64_t *sequence, int n)
{
    if (sequence[n] != -1)
    {
        return sequence[n];
    }
    else
    {
        if (n == 0 || n == 1)
        {
            sequence[n] = n;
        }
        else
        {
            sequence[n] = fib_top_down(sequence, n - 1) + fib_top_down(sequence, n - 2);
        }
    }

    return sequence[n];
}

// Bottom up approach
// We use a data structure to simulate recursion
uint64_t bottom_up(int n)
{
    uint64_t sequence[n];

    int i;
    for (i = 0; i <= n; i++)
    {
        if (i == 0)
        {
            sequence[i] = 0;
        }
        else if (i == 1)
        {
            sequence[i] = 1;
        }
        else
        {
            sequence[i] = sequence[i - 1] + sequence[i - 2];
        }
    }

    return sequence[n];
}
