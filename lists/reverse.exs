defmodule Reverse do
  def rev([]), do: []
  def rev([head | tail]) do
      rev(tail) ++ [head]
  end

  def tail_rev(l), do: tail_rev(l, [])
  defp tail_rev([], acc), do: acc
  defp tail_rev([head | tail], acc) do
    tail_rev(tail, [head | acc])
  end 
end
