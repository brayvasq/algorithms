%% Modules must be named the same as the file name
-module(size).
-export([len/1, tail_len/1]).

len([]) -> 0;
%% Erlang defines lists recursively
%% as [1 | [ 2 | ...[n |[]]]]
%% So ce can use the [H|T] partern to match 
%% against lists.
len([_ | T]) -> 1 + len(T).

%% Tail implementation
tail_len(L) -> tail_len(L, 0).

tail_len([], Acc) -> Acc;
tail_len([_|T], Acc) -> tail_len(T, Acc + 1).
