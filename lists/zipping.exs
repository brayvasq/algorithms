defmodule Zipping do
  # The zip function take two lists and combine the elements in tuples
  # Returning a list of tuples {X, Y}:
  #  [a,b,c] [1,2,3]
  # The zip function will be
  #  [{a, 1}, {b,2} , {c,3}]
  def zip([], []), do: []
  def zip([x | xs], [y | ys]) do
    [{x,y}, zip(xs, ys)]
  end

  # The following implementation, works for lists
  # of different sizes, so ends if one of the two lists is dones
  def lenient([], _), do: []
  def lenient(_, []), do: []
  def lenient([x|xs], [y|ys]) do
    [{x,y} | lenient(xs, ys)]
  end
end
