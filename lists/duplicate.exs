defmodule Duplicate do
  # Creates a copy of a list N times
  # Example:
  #   dup(3, [1,2,3])
  #  [[1,2,3],[1,2,3],[1,2,3]]
  def dup(0, _), do: []
  def dup(n, term) when n > 0 do
    [term | dup(n-1, term)]
  end

  # Tail implementation
  def tail_dup(n, term), do: tail_dup(n, term, [])
  defp tail_dup(0, _, list), do: list
  defp tail_dup(n, term, list) when n > 0 do
    tail_dup(n - 1, term, [term | list])
  end
end
