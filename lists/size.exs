defmodule Size do
  # As Erlang, in elixir we can define lists recursively
  # as [1 | [ 2 | ...[n |[]]]]
  # So ce can use the [H|T] partern to match 
  # against lists.
  #
  # To test the functions we can use comprehensions
  # to generate lists:
  #   for n <- [1, 2, 3, 4], do: n * n
  # Or use ranges like ruby
  #     1..10
  #     Enum.to_list(1..10)
  def len([]), do: 0
  def len([_|tail]) do
    1 + len(tail)
  end

  # Tail implementation
  def tail_len(l), do: tail_len(l, 0)
  defp tail_len([], acc), do: acc
  defp tail_len([_|tail], acc) do
      tail_len(tail, acc + 1)
  end
end
