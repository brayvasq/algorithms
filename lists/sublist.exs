defmodule Sublist do
  def sub(_, 0), do: []
  def sub([], _), do: []
  def sub([head|tail], n) when n > 0 do
    [head | sub(tail, n - 1)]
  end

  # Tail implementation
  def tail_sub(l, n), do: tail_sub(l, n, [])
  # This function works as an accumulator
  # as the reverse function. So we need to 
  # apply the reverse function to the result.
  defp tail_sub(_, 0, sub_list), do: sub_list
  defp tail_sub([], _, sub_list), do: sub_list
  defp tail_sub([head|tail], n, sub_list) do
    tail_sub(tail, n-1, [head | sub_list])
  end
end
