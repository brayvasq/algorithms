# List length
To create a recursive function we need:
- a base case.
- a function that calss itself.
- a list to try our function on.

```bash
# Erlang defines lists recursively as [1 | [ 2 | ...[n |[]]]]
# So ce can use the [H|T] partern to match against lists.
# We have that:
len([]) -> 0   # this is the base case
len([_ | T]) -> 1 + len(T).

len([1,2,3,4]) = len([1 | [2,3,4])
               = 1 + len([2 | [3,4]])
               = 1 + 1 + len([3 | [4]])
               = 1 + 1 + 1 + len([4 | []])
               = 1 + 1 + 1 + 1 + len([])
               = 1 + 1 + 1 + 1 + 0
               = 1 + 1 + 1 + 1
               = 1 + 1 + 2
               = 1 + 3 
               = 4


# List comprehesions to test the implementation
size:len([X || X <- lists:seq(1, 1000)]).
size:len([X * 2 || X <- [1,2,3,4,5]]).


# Tail implementation
# To reduce the memory usage.
tail_len(L) -> tail_len(L, 0).
tail_len([], Acc) -> Acc.
tail_len([_ | T]) -> tail_len(T, Acc + 1).

size:tail_len([X || X <- lists:seq(1, 1000)]).
size:tail_len([X * 2 || X <- [1,2,3,4,5]]).
```

# List duplicate
```bash
duplicate(0, _) -> [];
duplicate(N, Term) when N > 0 ->
   [Term | duplicate(N-1, Term)]

dup(3, [1,2,3]) = [[1,2,3] | dup(2, [1,2,3])]
                = [[1,2,3] | [[1,2,3] | dup(1, [1,2,3])]]
                = [[1,2,3] | [[1,2,3] | [[1,2,3] | dup(0, [1,2,3])]]
                = [[1,2,3] | [[1,2,3] | [[1,2,3] | []]]]
                = [[1,2,3] | [[1,2,3] | [[1,2,3]]]]
                = [[1,2,3] | [[1,2,3] , [1,2,3]]]
                = [[1,2,3], [1,2,3], [1,2,3]]

# Tail implementation
tail_dup(N, Term) -> 
    tail_dup(N, Term, []).

tail_dup(0, _, List) ->
    List;
tail_dup(N, Term, List) when N > 0 ->
    tail_dup(N-1, Term, [Term|List]).

# Imperative equivalent
function(N, Term) ->
  while N > 0 ->
    List = [Term | List],
    N = N - 1
  end,
  List.
```

# List reverse
```bash
rev([]) -> [];
rev([H|T]) -> rev(T) ++ [H].

rev([1,2,3,4]) = rev([2,3,4]) ++ [1]
               = rev([3, 4]) ++ [2] ++ [1]
               = rev([4]) ++ [3] ++ [2] ++ [1]
               = rev([]) ++ [4] ++ [3] ++ [2] ++ [1]
               = [] ++ [4] ++ [3] ++ [2] ++ [1]

# Tail implementation
tail_rev(L) -> tail_rev(L, []).

tail_rev([], Acc) -> Acc;
tail_rev([H|T], Acc) -> tail_rev(T, [H | Acc]).

tail_rev([1,2,3,4]) = tail_rev([1,2,3,4], [])
                    
tail_rev([1 | [2,3,4]], []) = tail_rev([2 | [3,4]], [1 | []])
                            = tail_rev([3 | [4]], [2 | [1 | []]])
                            = tail_rev([4], [3 | [2 | [1 | []]]])
                            = tail_rev([], [4 | [3 | [2 | [1 | []]]]])
                            = [4 | [3 | [2 | [1 | []]]]]
```

# List sublist
```bash
sub(_, 0) -> [];
sub([], _) -> [];
sub([H|T], N) when N > 0 -> [H | sub(T, N-1)].

sub([1,2,3,4,5,6], 4) = sub([1 | [2,3,4,5,6]], 4)
                      = [1 | sub([2 | [3,4,5,6], 3])]
                      = [1 | [2 | sub([3 | [4,5,6]], 2)]]
                      = [1 | [2 | [3 | sub([4 | [5,6]], 1)]]]
                      = [1 | [2 | [3 | [4 | sub([5 | 6], 0)]]]]
                      = [1 | [2 | [3 | [4 | []]]]]

# Tail implementation
tail_sub(L, N) -> tail_sub(L, N, []).

tail_sub(_, 0, SubList) -> SubList;
tail_sub([], _, SubList) -> SubList;

# This function works as an accumulator
# as the reverse function. So we need to 
# apply the reverse function to the result.
tail_sub([H|T], N, SubList) when N > 0 ->
    tail_sub(T, N-1, [H | SubList]).
```

# List zip
```bash
# The zip function take two lists
# And combine the elements in tuples
# Returning a list of tuples {X, Y}:
#  [a,b,c] [1,2,3]
# The zip function will be
#  [{a, 1}, {b,2} , {c,3}]
```

# References
https://learnyousomeerlang.com/recursion#hello-recursion
