-module(reverse).
-export([rev/1, tail_rev/1]).

rev([]) -> [];
rev([H|T]) -> rev(T) ++ [H].

tail_rev(L) -> tail_rev(L, []).

tail_rev([], Acc) -> Acc;
tail_rev([H|T], Acc) -> tail_rev(T, [H | Acc]).
