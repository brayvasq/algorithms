-module(sublist).
-export([sub/2, tail_sub/2]).

sub(_, 0) -> [];
sub([], _) -> [];
sub([H|T], N) when N > 0 -> [H | sub(T, N-1)].

%% Tail implementation
tail_sub(L, N) -> tail_sub(L, N, []).

tail_sub(_, 0, SubList) -> SubList;
tail_sub([], _, SubList) -> SubList;

%% This function works as an accumulator
%% as the reverse function. So we need to 
%% apply the reverse function to the result.
tail_sub([H|T], N, SubList) when N > 0 ->
    tail_sub(T, N-1, [H | SubList]).

