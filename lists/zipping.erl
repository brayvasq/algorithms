-module(zipping).
-export([zip/2, lenient/2]).

%% The zip function take two lists and combine the elements in tuples
%% Returning a list of tuples {X, Y}:
%%  [a,b,c] [1,2,3]
%% The zip function will be
%%  [{a, 1}, {b,2} , {c,3}]
zip([],[]) -> [];
zip([X|Xs], [Y|Ys]) -> [{X,Y} | zip(Xs, Ys)].

%% The following implementation, works for lists
%% of different sizes, so ends if one of the two lists is dones
lenient([], _) -> [];
lenient(_, []) -> [];
lenient([X|Xs], [Y|Ys]) -> [{X,Y} | lenient(Xs, Ys)].
